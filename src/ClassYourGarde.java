import java.util.Scanner;

public class ClassYourGarde {
    public static void main(String[] args) {
        String strScore;
        int score;
        String garde;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please Input Your score: ");
        strScore = sc.next();
        // System.out.println(strScore);
        score = Integer.parseInt(strScore);
        // System.out.println(strScore);
        
        if(score>=80){
            garde = "A";
        }else if(score>=75){
            garde = "B+";
        }else if(score>=70){
            garde = "B";
        }else if(score>=65){
            garde = "C+";
        }else if(score>=60){
            garde = "C";
        }else if(score>=55){
            garde = "D+";
        }else if(score>=50){
            garde = "D";
        }else {
            garde = "F";
        }
        System.out.println("Garde " + garde);

    }
}
